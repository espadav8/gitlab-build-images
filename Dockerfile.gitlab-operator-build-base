FROM golang:1.14

LABEL maintainer="GitLab Distribution Team" \
      description="Build base image for the GitLab Operator project."

ARG KUBEBUILDER_VERSION=2.3.1
ARG YQ_VERSION=4.7.0
ARG HELM_VERSION=3.5.2

RUN go get -u \
        github.com/onsi/ginkgo/ginkgo \
        golang.org/x/lint/golint \
    && curl --retry 6 -Ls https://go.kubebuilder.io/dl/${KUBEBUILDER_VERSION}/linux/amd64 | tar -xz -C /tmp/ \
    && mv /tmp/kubebuilder_${KUBEBUILDER_VERSION}_linux_amd64 /usr/local/kubebuilder \
    && ln -sfv /usr/local/kubebuilder/bin/* /usr/local/bin \
    && curl --retry 6 -LsO https://github.com/mikefarah/yq/releases/download/v${YQ_VERSION}/yq_linux_amd64 \
    && chmod +x yq_linux_amd64 \
    && mv yq_linux_amd64 /usr/local/bin/yq \
    && curl --retry 6 -Ls "https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar -xz -C /tmp/ \
    && chmod +x /tmp/linux-amd64/helm \
    && mv /tmp/linux-amd64/helm /usr/local/bin/helm \
    && rm -rf /var/lib/apt/lists/* /tmp/*

