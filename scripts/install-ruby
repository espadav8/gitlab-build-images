#!/bin/bash

set -xeou pipefail

# Based on https://github.com/docker-library/ruby/blob/master/2.7/buster/Dockerfile

RUBY_VERSION=${1}
RUBY_MAJOR=${1%.*} # strip last component
RUBY_DOWNLOAD_SHA256=${2}
RUBY_DOWNLOAD_URL="https://cache.ruby-lang.org/pub/ruby/${RUBY_MAJOR%-rc}/ruby-$RUBY_VERSION.tar.gz"

JEMALLOC_VERSION=5.2.1
JEMALLOC_DOWNLOAD_SHA256="34330e5ce276099e2e8950d9335db5a875689a4c6a56751ef3b1d8c537f887f6"
JEMALLOC_DOWNLOAD_URL="https://github.com/jemalloc/jemalloc/releases/download/${JEMALLOC_VERSION}/jemalloc-${JEMALLOC_VERSION}.tar.bz2"

# Install needed packages
apt-get update
apt-get install -y --no-install-recommends bison dpkg-dev libgdbm-dev autoconf

# Download jemalloc
mkdir -p /usr/src/jemalloc
cd /usr/src/jemalloc
curl --retry 6 -L -so jemalloc.tar.bz2 ${JEMALLOC_DOWNLOAD_URL}
echo "${JEMALLOC_DOWNLOAD_SHA256} jemalloc.tar.bz2" | sha256sum -c -

# Install jemalloc
tar -xjf jemalloc.tar.bz2
rm jemalloc.tar.bz2
cd jemalloc-${JEMALLOC_VERSION}
./autogen.sh --prefix=/usr --enable-prof
make -j "$(nproc)" install
cd /tmp

# Download Ruby
curl -fsSL "$RUBY_DOWNLOAD_URL" -o ruby.tar.gz
echo "${RUBY_DOWNLOAD_SHA256} ruby.tar.gz" | sha256sum -c -

# Skip installing Gem docs
mkdir -p /usr/local/etc
echo 'install: --no-document' >> /usr/local/etc/gemrc
echo 'update: --no-document' >> /usr/local/etc/gemrc

# Unpack Ruby
mkdir -p /usr/src/ruby
tar -xzf ruby.tar.gz -C /usr/src/ruby --strip-components=1
rm ruby.tar.gz
cd /usr/src/ruby

# Apply patches
if [[ -d "/patches/ruby/$RUBY_VERSION" ]]; then
  for i in "/patches/ruby/$RUBY_VERSION"/*.patch; do
    echo "$i..."
    patch -p1 -i "$i"
  done
fi

# Compile
cflags="-fno-omit-frame-pointer" ./configure --enable-shared --with-jemalloc --disable-install-doc --disable-install-rdoc --disable-install-capi
make install -j $(nproc)

# Cleanup
cd /
rm -rf /usr/src/ruby /usr/src/jemalloc
apt-get purge -y --auto-remove ruby

# Verify
# verify we have no "ruby" packages installed
! dpkg -l | grep -i ruby
[ "$(command -v ruby)" = '/usr/local/bin/ruby' ]

# rough smoke test
ruby --version
gem --version
bundle --version
