#!/bin/bash

set -xeuo pipefail
IFS=$'\n\t'

export DEBIAN_FRONTEND=noninteractive

# echo "deb http://deb.debian.org/debian testing main" | tee -a /etc/apt/sources.list.d/testing.list
# Install LaTeX and other packages
apt-get update
apt-get install -yq --no-install-recommends \
  make gcc g++ locales \
  rsync git-core texlive-latex-recommended texlive-xetex \
  texlive-fonts-recommended lmodern ed file curl gnupg2 \
  unzip \
  python3 python3-pip python3-crcmod

# Install pandoc
cd /tmp
curl -L -O https://github.com/jgm/pandoc/releases/download/2.3.1/pandoc-2.3.1-linux.tar.gz
tar xvf pandoc-2.3.1-linux.tar.gz
cp pandoc-2.3.1/bin/* /usr/local/bin
rm -rf /tmp/pandoc*

# Install Imagemagick for cropping the pictures on the team page
apt-get install -yq --no-install-recommends imagemagick

# Install node & yarn
NODE_INSTALL_VERSION=12.22.1
YARN_INSTALL_VERSION=1.22.10
/scripts/install-node $NODE_INSTALL_VERSION $YARN_INSTALL_VERSION && node --version && yarn --version

# Install yamllint
# We need version 1.25.0+: https://github.com/adrienverge/yamllint/blob/master/CHANGELOG.rst
YAMLLINT_VERSION=1.25.0

# Temporarily pin pyyaml and pathspec to reduce compatibility with packages from Debian bullseye (testing)
pip3 install yamllint==${YAMLLINT_VERSION} pyyaml==5.4.1 pathspec==0.8.1

# Install gitlab-runner
curl -O -J -L https://gitlab-ci-multi-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-ci-multi-runner-linux-amd64
mv gitlab-ci-multi-runner-linux-amd64 /usr/bin/gitlab-runner-helper
chmod +x /usr/bin/gitlab-runner-helper

# Set UTF-8
echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
locale-gen
update-locale LANG=en_US.UTF-8 LC_CTYPE=en_US.UTF-8 LC_ALL=en_US.UTF-8
locale -a

# Clean up
apt-get autoremove -yq
apt-get clean -yqq
rm -rf /var/lib/apt/lists/*
