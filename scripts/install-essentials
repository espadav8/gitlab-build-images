#!/bin/bash

set -xeuo pipefail
IFS=$'\n\t'

export DEBIAN_FRONTEND=noninteractive

apt-get update

# We install `git-core` as some tooling expect `/usr/bin/git`
# other tools that rely on PATH ordering will pick a one in `/usr/local`
# if present

if grep "Debian GNU/Linux 9" /etc/issue
then
    apt-get install -y \
            curl wget build-essential apt-utils locales openssh-client \
            libssl-dev libyaml-dev libreadline6-dev zlib1g-dev \
            libncurses5-dev libffi-dev libgdbm3 libgdbm-dev \
            ca-certificates checkinstall libxml2-dev \
            libxslt-dev libcurl4-openssl-dev libicu-dev \
            logrotate python-docutils pkg-config cmake nodejs \
            libkrb5-dev postgresql-client mysql-client unzip \
            libsqlite3-dev libpq-dev libpng-dev libjpeg-dev libzstd-dev \
            libre2-dev libevent-dev gettext rsync git-core
else
    apt-get install -y \
            curl wget build-essential apt-utils locales openssh-client \
            libssl-dev libyaml-dev libreadline-dev zlib1g-dev \
            libncurses5-dev libffi-dev ca-certificates libxml2-dev \
            libxslt1-dev libcurl4-openssl-dev libicu-dev \
            logrotate python-docutils pkg-config cmake \
            libkrb5-dev postgresql-client unzip \
            libsqlite3-dev libpq-dev libpng-dev libjpeg-dev libzstd-dev \
            libre2-dev libevent-dev gettext rsync git-core
fi

# Set UTF-8
# http://stackoverflow.com/a/3182519/2137281
LOC=$'LC_ALL=en_US.UTF-8\nLANG=en_US.UTF-8'
echo "$LOC" > /etc/environment
cat /etc/environment
echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
locale-gen
dpkg-reconfigure locales -f noninteractive -p critical
locale -a

apt-get autoremove -yq
apt-get clean -yqq
rm -rf /var/lib/apt/lists/*
